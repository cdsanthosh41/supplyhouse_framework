package Stepdefinition;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import pages.searchPage;
import utils.stringOperations;

//Created by Santhosh-11/11/2020
/*Stepdefinition corresponding to searchResult feature*/

public class searchResult extends searchPage {

    WebDriver driver;
    stringOperations sto=new stringOperations();

    @Given("user loads url")
    public void user_loads_url() throws InterruptedException {
        driver.get(searchUrl);
        Thread.sleep(1000);
        driver.manage().window().maximize();
    }

    @When ("^user search ([^\"]*)$")
    public void user_search(String modelnumber) {
        driver.findElement(modelNumber).sendKeys(modelnumber);
        driver.findElement(searchField).click();

    }

    @Then("^search result displays entered ([^\"]*)$")
    public void search_result_displays_entered(String modelnumber) throws InterruptedException {
        Thread.sleep(1000);
        String element =driver.findElement(skuField).getText();
        String[] splitelement=sto.splitOperations(element);
        Assert.assertEquals(splitelement[1],modelnumber,"Incorrect model number displayed");

    }

    @Before
//    Starting chrome browser session
    public WebDriver startBrowser(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver=new ChromeDriver();
        return driver;
    }

    @After
    //    close chrome browser session
    public void closeBrowser(){
        driver.quit();
    }



}
