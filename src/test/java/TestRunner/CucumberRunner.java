package TestRunner;


import io.cucumber.java.Before;
import io.cucumber.junit.Cucumber;
import io.cucumber.testng.AbstractTestNGCucumberTests;
import org.junit.runner.RunWith;
import io.cucumber.testng.CucumberOptions;
import org.testng.annotations.BeforeClass;

//Created by Santhosh-11/11/2020
//cucumber runner and options

@RunWith(Cucumber.class)

@CucumberOptions(
        features ="src/test/java/Features",
        glue = "Stepdefinition",
        plugin = {"html:target/site/cucumber-pretty.html"},
        monochrome = true

)

public class CucumberRunner extends AbstractTestNGCucumberTests {

}



