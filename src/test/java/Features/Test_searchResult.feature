Feature: Test_searchResult
  Test for Whether search result displays entered model number

  Scenario Outline: Verify search result displays entered model number
    Given user loads url
    When user search <modelnumber>
    Then search result displays entered <modelnumber>
    Examples:
    |modelnumber|
    |AT082410C|
