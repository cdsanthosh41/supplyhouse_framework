package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

//Created by Santhosh-11/11/2020
public class searchPage {

//  page url
    public String searchUrl="https://www.supplyhouse.com/Boiler-Parts-Finder-Tool";
//    page object identifier
    public By modelNumber= By.id("model-number");
    public By searchField= By.xpath("//*[@id='boiler-model-number-search']/button");
    public By skuField= By.xpath("//*[@class='desc-sku']");

//    getter methods
    public By getModelnumber()
{
    return modelNumber;
}
    public By getsearchField()
    {
        return searchField;
    }
    public By skuField()
    {
        return skuField;
    }

}
